BEGIN;

CREATE TABLE taxi_positions
(
  id integer,
  observ timestamp without time zone,
  lng double precision,
  lat double precision
);


CREATE INDEX taxis_observ_idx
  ON taxi_positions
  USING btree
  (observ);


CREATE INDEX taxis_id_idx
ON taxi_positions
USING btree(id);


INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:04:02',116.7539,40.02126);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:09:01',116.7539,40.02126);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:14:02',116.7539,40.02126);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:19:02',116.7539,40.02126);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:34:02',116.75033,40.05878);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:39:02',116.70075,40.0606);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:44:02',116.70035,40.05369);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:44:02',116.70035,40.05369);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:49:02',116.67169,40.03407);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:54:01',116.67032,39.99409);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 09:59:01',116.65041,39.9481);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:04:01',116.65199,39.93277);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:09:01',116.63368,39.92585);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:14:01',116.59928,39.92505);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:19:01',116.58457,39.92077);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:24:01',116.58454,39.92062);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:29:01',116.58465,39.9206);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:34:02',116.5849,39.9205);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:39:02',116.58475,39.92016);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:54:01',116.55815,39.92311);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 10:59:01',116.51462,39.92251);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:04:01',116.49104,39.91451);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:09:01',116.48706,39.91012);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:14:01',116.4837,39.91426);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:14:06',116.4837,39.91426);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:19:01',116.46639,39.91447);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:24:15',116.44702,39.92005);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:29:01',116.44056,39.92205);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:39:01',116.45103,39.92714);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:44:01',116.455,39.93257);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:44:01',116.455,39.93257);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:49:01',116.47199,39.93245);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:49:01',116.47199,39.93245);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:49:01',116.47199,39.93245);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:54:01',116.48602,39.93182);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 11:59:01',116.50056,39.93613);
INSERT INTO taxi_positions VALUES (3,'2008-02-03 12:04:01',116.53189,39.9403);

COMMIT;
