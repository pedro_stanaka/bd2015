/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Usuario;

/**
 *
 * @author pedro
 */
@WebServlet(name = "UsuarioController",
        urlPatterns = {
            "/usuario",
            "/usuario/create",
            "/usuario/update",
            "/usuario/delete"
        })
public class UsuarioController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;

        DAOFactory daoFactory;
        UsuarioDAO usuarioDao;

        switch (request.getServletPath()) {
            case "/usuario": {
                try {
                    daoFactory = new DAOFactory();

                    usuarioDao = daoFactory.getUsuarioDAO();

                    List<Usuario> usuarios = usuarioDao.all();

                    request.setAttribute("usuarios", usuarios);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }

            case "/usuario/create": {
                requestDispatcher = request.getRequestDispatcher("/view/usuario/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/usuario/update": {
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();

                    int id = Integer.parseInt(request.getParameter("id"));

                    Usuario usuario = usuarioDao.read(id);

                    request.setAttribute("usuario", usuario);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/update.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAOFactory daoFactory;
        UsuarioDAO dao;
        switch (request.getServletPath()) {
            case "/usuario/create": {
                Usuario usuario = new Usuario();

                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("datanasc"));

                    usuario.setDataNascimento(new java.sql.Date(dataNascimento.getTime()));

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.create(usuario);

                    response.sendRedirect(request.getContextPath() + "/usuario");

                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/usuario/update": {

                Usuario usuario = new Usuario();

                usuario.setId(Integer.parseInt(request.getParameter("id")));
                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("datanasc"));

                    usuario.setDataNascimento(new java.sql.Date(dataNascimento.getTime()));

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.update(usuario);

                    response.sendRedirect(request.getContextPath() + "/usuario");

                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/usuario/delete": {

                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    
                    int id = Integer.parseInt(request.getParameter("id"));
                    
                    dao.delete(id);                    
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
