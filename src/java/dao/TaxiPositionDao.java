/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.TaxiPosition;

/**
 *
 * @author pedro
 */
public class TaxiPositionDao extends DAO<TaxiPosition> {

    private final String queryRandom = "SELECT lat, lng, id, observ "
            + "FROM taxi_positions "
            + "WHERE id = 3 AND "
            + "   observ BETWEEN '2008-02-03 09:00:00.000000'::TIMESTAMP WITHOUT TIME ZONE "
            + "       AND '2008-02-03 12:05:00.000000'::TIMESTAMP WITHOUT TIME ZONE "
            + "ORDER BY observ ASC;";

    public TaxiPositionDao(Connection connection) {
        super(connection);
    }

    @Override
    public void create(TaxiPosition obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TaxiPosition read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(TaxiPosition obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TaxiPosition> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<TaxiPosition> randomTrajectory() {
        try {
            PreparedStatement statement = connection.prepareStatement(queryRandom);
            ResultSet result = statement.executeQuery();

            List<TaxiPosition> positions = new ArrayList<>();
            while (result.next()) {
                TaxiPosition position = new TaxiPosition();

                position.setId(result.getInt("id"));
                position.setObserv(result.getTimestamp("observ"));
                position.setLat(result.getDouble("lat"));
                position.setLng(result.getDouble("lng"));

                positions.add(position);
            }

            return positions;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
