/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

/**
 *
 * @author pedro
 */
public class UsuarioDAO extends DAO<Usuario> {
    
    private final String createQuery = "INSERT INTO usuario(nome, login, senha, nascimento) "
            + "VALUES (?, ?, md5(?), ?) RETURNING id;";
    
    private final String allQuery = "SELECT id, nome FROM usuario;";
    
    private final String readQuery = "SELECT * FROM usuario WHERE id = ?;";
    
    private final String updateQuery = "UPDATE usuario "
                                     + "SET nome = ?, login = ?, senha = md5(?), nascimento = ? "
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM usuario WHERE id = ?;";
    
    private final String authenticateQuery = "SELECT * "
                                           + "FROM usuario "
                                            + "WHERE login = ? AND senha = md5(?);";
    

    public UsuarioDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, usuario.getNome());
            statement.setString(2, usuario.getLogin());
            statement.setString(3, usuario.getSenha());
            statement.setDate(4, usuario.getDataNascimento());
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Usuario read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDataNascimento(result.getDate("nascimento"));
            }
            
            return usuario;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
        
    }

    @Override
    public void update(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, usuario.getNome());
            statement.setString(2, usuario.getLogin());
            statement.setString(3, usuario.getSenha());
            statement.setDate(4, usuario.getDataNascimento());
            
            statement.setInt(5, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<Usuario>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public Usuario authenticate(String login, String password) {
         try {
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            
            statement.setString(1, login);
            statement.setString(2, password);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDataNascimento(result.getDate("nascimento"));
                return usuario;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }

    
    
}
