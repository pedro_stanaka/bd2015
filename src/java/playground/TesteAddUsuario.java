/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package playground;

import dao.DAOFactory;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

/**
 *
 * @author pedro
 */
public class TesteAddUsuario {
    
    public static void main(String[] args) {
        try {
            DAOFactory daoFactory = new DAOFactory();
            
            UsuarioDAO dao = daoFactory.getUsuarioDAO();
            
            Usuario usuario = new Usuario();
            
            usuario.setLogin("pedro1");
            usuario.setNome("Pedro Tanaka");
            usuario.setSenha("pedro");
            
            Calendar calendar = Calendar.getInstance();
            calendar.set(1992, 4, 30);
            Date data = new Date(calendar.getTimeInMillis());
            
            
            usuario.setDataNascimento(data);
            
            dao.create(usuario);
            
            System.out.println("OK");
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TesteAddUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TesteAddUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TesteAddUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
