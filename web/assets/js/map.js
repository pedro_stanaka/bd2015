function createMarker(position, map) {
    var marker = L.marker([position.lat, position.lng]).addTo(map);
    var message = "<p><strong>Taxi ID: </strong>"+ position.id +"</p>";
    message += "<p><strong>Time: </strong>"+ position.observ +"</p>";
    marker.bindPopup(message);
    return marker;
}

$(document).ready(function() {

    var map = L.map('my-map').setView([40.02133 , 116.75389], 9);

    var osmUrl='http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});
    map.addLayer(osm);


    var myMarkers = [];
    var polyline;
    $.get($.url("/taxis/randomtraj"), function (positions) {
        for (var i = 0; i < positions.length; ++i) {
            myMarkers.concat(createMarker(positions[i], map));
        }
        polyline = L.polyline(positions, {color: 'green', opacity: 0.8}).addTo(map);
    });
    


});