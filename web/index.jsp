<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/usuario" />
</c:if>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Stylesheets -->

        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">Você está usando um navegador <strong>desatualizado</strong>. Por favor, <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar sua experiência.</p>
        <![endif]-->
        <%@include file="/view/includes/menu.jsp"%>

        <div class="container">
            <!-- Conteúdo -->

            <div class="row">
                <div class="col-md-10">
                    
                </div>
            </div>
        </div>


        <!-- FIM -- Conteúdo -->
        <hr>

        <footer>
            <p>&copy; UEL - 2015</p>
        </footer>
    </div> <!-- /container -->        

    <!-- Scripts JavaScript, carregando no final da página para melhorar tempo de renderização. -->
    <%@include file="/view/includes/scripts.jsp" %>
</body>
</html>
