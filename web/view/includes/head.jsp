<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/bootstrap.min.css" />
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/main.css" />

<base href="${pageContext.servletContext.contextPath}" />