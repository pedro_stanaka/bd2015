<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">BD 2015</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.servletContext.contextPath}/">�nicio</a></li>
                    <c:if test="${not empty sessionScope.usuario}">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Usu�rio <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="${pageContext.servletContext.contextPath}/usuario">Listagem</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/usuario/create">Cadastro</a></li>
                        </ul>
                    </li>
                </c:if>
            </ul>
            <div class="navbar-right">
                <c:choose>
                    <c:when test="${not empty sessionScope.usuario}" >
                        <a href="${pageContext.servletContext.contextPath}/logout" class="btn btn-danger" />Logout</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${pageContext.servletContext.contextPath}/login" class="btn btn-success" />Login</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div><!--/.nav-collapse -->


    </div>
</nav>