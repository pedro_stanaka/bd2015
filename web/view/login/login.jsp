<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/usuario" />
</c:if>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt"> <!--<![endif]-->
    <head>
        
        <title></title>

        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">Você está usando um navegador <strong>desatualizado</strong>. Por favor, <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar sua experiência.</p>
        <![endif]-->


        <div class="container">
            <!-- Conteúdo -->

            <div class="row">
                <div class="col-md-10">
                    <form method="post" action="${pageContext.servletContext.contextPath}/login" >
                        <div class="form-group">
                            <label>Login</label>
                            <input type="text" name="login" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Senha</label>
                            <input type="password" name="senha" class="form-control"/>
                            
                        </div>

                        <input type="submit" value="Login" class="btn btn-primary" />
                        
                    </form>
                        <p class="help-block">Não é cadastrado? <a href="${pageContext.servletContext.contextPath}/usuario/create">Faça seu cadastro</a></p>
                </div>
            </div>
        </div>


        <!-- FIM -- Conteúdo -->
        <hr>

        <footer>
            <p>&copy; UEL - 2015</p>
        </footer>
    </div> <!-- /container -->        

    <!-- Scripts JavaScript, carregando no final da página para melhorar tempo de renderização. -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>

    <script src="assets/js/bootstrap.min.js"></script>

    <script src="js/main.js"></script>

</body>
</html>
