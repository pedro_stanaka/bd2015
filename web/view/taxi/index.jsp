<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <%@include file="/view/includes/head.jsp"%>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>
    <title>[BD 2015] Taxis</title>
</head>
<body>
    <%@include file="/view/includes/menu.jsp"%>

<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div id="my-map">

            </div>
        </div>
    </div>

</div> <!-- container -->

<%@include file="/view/includes/scripts.jsp"%>

<script src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/map.js"></script>
</body>
</html>