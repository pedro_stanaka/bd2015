<%-- 
    Document   : index
    Created on : 22/10/2015, 17:01:53
    Author     : pedro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>

        <title>Listagem Usuários</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Listagem dos usuários</h1>
                </div>
                <div class="col-md-2 col-xs-2">
                    <a href="${pageContext.servletContext.contextPath}/usuario/create" class="right btn btn-primary">Criar novo</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${usuarios}" var="u">
                                <tr>
                                    <td>${u.id}</td>
                                    <td>${u.nome}</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/usuario/update?id=${u.id}">Editar</a>
                                        |
                                        <a href="#" class="link-delete" data-id="${u.id}" >Deletar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <%@include file="/view/includes/scripts.jsp" %>

    </body>
</html>
