<%-- 
    Document   : create
    Created on : 22/10/2015, 17:30:01
    Author     : pedro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <title>Update</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de usuário</h1>
        
        <form method="post" action="${pageContext.servletContext.contextPath}/usuario/update" >
            
            <input type="hidden" name="id" value="${usuario.id}" />
            
            <div class="form-group">
                <label>Nome:</label>
                <input type="text" name="nome" class="form-control" value="${usuario.nome}" />
            </div>
            
            <div class="form-group">
                <label>Login: </label>
                <input type="text" name="login" class="form-control" value="${usuario.login}" />
            </div>
            
            
            <div class="form-group">
                <label>Senha: </label>
                <input type="password" name="senha" class="form-control" value="${usuario.senha}" />
            </div>
            
            <div class="form-group">
                <label>Data nascimento:</label>
                <input type="text" name="datanasc" class="form-control"  value="${usuario.dataNascimento}" />
            </div>
            
            <input type="submit" value="Enviar" class="btn btn-primary" />
        </form>
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
